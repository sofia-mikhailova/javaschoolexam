package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    private final String OPERATORS = "+-/*";

    private Stack<String> stackOperations = new Stack<>();
    private Stack<String> stackRPN = new Stack<>();
    private Stack<String> stackAnswer = new Stack<>();

    private boolean isOperator(String token) {
        return OPERATORS.contains(token);
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private int getPrecedence(String token) {
        if (token.equals("+") || token.equals("-")) {
            return 1;
        } else {
            return 2;
        }
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            checkInput(statement);
            toRPN(statement);
            return evaluate();
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    private void checkInput(String statement) {
        if (statement == null || statement.equals("")) throw new IllegalArgumentException("");
        if (statement.indexOf("++") > 0 || statement.indexOf("--") > 0
                || statement.indexOf("**") > 0 || statement.indexOf("//") > 0
                || statement.indexOf("..") > 0 || statement.indexOf(",") > 0) throw new IllegalArgumentException("");
    }

    private String evaluate() {
        while (!stackRPN.empty()) {
            String token = stackRPN.pop();
            if (isNumber(token)) {
                stackAnswer.push(token);
            } else if (isOperator(token)) {
                String firstNumber = stackAnswer.pop();
                String secondNumber = stackAnswer.pop();
                stackAnswer.push(doOperation(firstNumber, secondNumber, token));
            }
        }
        String answer = stackAnswer.pop();

        if (answer.endsWith(".0")) {
            answer = answer.substring(0, answer.indexOf('.'));
        }

        return answer;
    }

    private String doOperation(String firstNumber, String secondNumber, String operation) {
        Double first = Double.parseDouble(firstNumber);
        Double second = Double.parseDouble(secondNumber);
        Double result = 0.0;
        switch (operation) {
            case "+": {
                result = first + second;
                break;
            }
            case "-": {
                result = second - first;
                break;
            }
            case "*": {
                result = first * second;
                break;
            }
            case "/": {
                result = second / first;
                break;
            }
        }
        if (result.isInfinite()) throw new IllegalArgumentException("");
        return result.toString();
    }

    private void toRPN(String statement) throws IllegalArgumentException {
        stackOperations.clear();
        stackRPN.clear();

        StringTokenizer stringTokenizer = new StringTokenizer(statement, OPERATORS + "()", true);

        while (stringTokenizer.hasMoreElements()) {
            String token = stringTokenizer.nextToken();

            if (isOpenBracket(token)) {
                stackOperations.push(token);
            } else if (isCloseBracket(token)) {
                if (!stackOperations.contains("(")) throw new IllegalArgumentException("");
                while (!stackOperations.empty() && !isOpenBracket(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                stackOperations.pop();
                if (!stackOperations.empty()) {
                    stackRPN.push(stackOperations.pop());
                }
                if (stackOperations.contains("(")) throw new IllegalArgumentException("");
            } else if (isNumber(token)) {
                stackRPN.push(token);
            } else if (isOperator(token)) {
                while (!stackOperations.empty() && isOperator(stackOperations.lastElement())
                        && getPrecedence(token) <= getPrecedence(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                stackOperations.push(token);
            }
        }
        while (!stackOperations.empty()) {
            stackRPN.push(stackOperations.pop());
        }

        Collections.reverse(stackRPN);
    }

}

package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Stack;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) throw new IllegalArgumentException();
        if (!x.isEmpty() && y.isEmpty()) return false;

        Stack<Integer> resultIndexes = new Stack<>();
        for (Object elem : x) {
            if (!resultIndexes.empty() && resultIndexes.firstElement() > y.indexOf(elem)) return false;
            resultIndexes.add(y.indexOf(elem));
        }
        return true;
    }
}

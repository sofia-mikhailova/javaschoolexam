package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        return createMatrix(inputNumbers, calculateHeight(inputNumbers.size()));
    }

    private int calculateHeight(int size) {
        int b = 1;
        int a = 1;
        int c = -2 * size;
        int d = b * b - 4 * a * c;

        Double x1 = (-b - Math.sqrt(d)) / (2 * a);
        Double x2 = (-b + Math.sqrt(d)) / (2 * a);

        if (x2 % 1 != 0) throw new CannotBuildPyramidException();

        return x2.intValue();
    }

    private int[][] createMatrix(List<Integer> inputNumbers, int height) {
        Collections.sort(inputNumbers);
        int width = height * 2 - 1;
        int leftBorder = width / 2;
        int rightBorder = leftBorder;
        int[][] result = new int[height][width];

        Iterator<Integer> iterator = inputNumbers.iterator();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((j >= leftBorder) && (j <= rightBorder)) {
                    result[i][j] = iterator.next();
                    j++;
                }
            }
            leftBorder--;
            rightBorder++;
        }

        return result;
    }


}
